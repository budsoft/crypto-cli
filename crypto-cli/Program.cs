﻿using CryptoProjectNetCore;
using System;
using System.Collections.Generic;
using static System.Linq.Enumerable;

namespace crypto_cli
{
	class Program
	{
		static void Main(string[] args)
		{
			if (args.Length < 3)
			{
				throw new ArgumentException("Missing argument");
			}

			string key = args[1];
			string data = ArgsToString(args.Skip(2));

			switch (args[0].ToLowerInvariant())
			{
				case "-e":
					Encrypt(key, data);
					break;
				case "-d":
					Decrypt(key, data);
					break;
				default:
					throw new ArgumentException($@"Invalid argument ""{args[0]}""");
			}
		}

		private static void Encrypt(string key, string str)
		{
			var crypto = new AESCrypto(key);
			Console.WriteLine(crypto.Encrypt(str));
		}

		private static void Decrypt(string key, string str)
		{
			var crypto = new AESCrypto(key);
			Console.WriteLine(crypto.Decrypt(str));
		}

		private static string ArgsToString(IEnumerable<string> args)
		{
			return String.Join(" ", args);
		}
	}
}